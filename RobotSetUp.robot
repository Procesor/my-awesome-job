*** Settings ***
Documentation    Suite description
Library  SeleniumLibrary
Library  String

*** Variables ***
${URL}              https://sandbox-business.revolut.com/signin
${BROWSER}          Chrome
${name}             jan.egermaier@tesena.com
${password}         Tesena2019.
${ExchangeInput}    10
*** Keywords ***
LogIN
    Open Browser    ${URL}  ${BROWSER}
    Maximize Browser Window
    Wait Until Element Is Visible       //*[@id="app"]/div/div[2]/div[1]/div/div[2]/div/div/h2
    Wait Until Element Is Visible       //*[@id="app"]/div/div[2]/div[1]/div/div[3]/div/div/div/div[1]/div[1]/div/div[1]/div[2]/input
    Input Text                          //*[@id="app"]/div/div[2]/div[1]/div/div[3]/div/div/div/div[1]/div[1]/div/div[1]/div[2]/input   ${name}
    Wait Until Element Is Visible       //*[@id="app"]/div/div[2]/div[1]/div/div[3]/div/div/div/div[1]/div[2]/label/div[2]/div/input
    Input Text                          //*[@id="app"]/div/div[2]/div[1]/div/div[3]/div/div/div/div[1]/div[2]/label/div[2]/div/input    ${password}
    Click Button                        //*[@id="app"]/div/div[2]/div[1]/div/div[4]/div/div/button
    Wait Until Element Is Visible       //*[@id="LeftSidebar"]/div/div/div/div[1]/div/header/div[2]
Click and Change Currency
    LogIN
    Wait Until Element Is Visible       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[1]/div[2]/div/div/div[1]/div/div/div/div[2]/span[1]/div/div[1]
    Click Element                       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[1]/div[2]/div/div/div[1]/div/div/div/div[2]/span[1]/div/div[1]
    Wait Until Element Is Visible       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[1]/div[2]/div/div/div[1]/div/div/div/div[2]/span[1]/div/div[2]
    Wait Until Element Is Visible       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[1]/div[2]/div/div/div[1]/div/div/div/div[2]/span[1]/div/div[2]/div[3]/div[2]/div/div
    ${USD}                              Get Text    //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[1]/div[2]/div/div/div[1]/div/div/div/div[2]/span[1]/div/div[2]/div[3]/div[2]/div/div
    Click Element                       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[1]/div[2]/div/div/div[1]/div/div/div/div[2]/span[1]/div/div[2]/div[3]/div[2]/div/div
    ${actual_currency}                  Get Text    //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[1]/div[2]/div/div/div[1]/div/div/div/div[2]/span[1]
    Should Be True                      """${USD}""" == """${actual_currency}"""
    Close
Top up functionality
    LogIn
    Wait Until Element Is Visible       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[1]/div[2]/div/div/div[2]/div/div[1]
    Click Element                       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[1]/div[2]/div/div/div[2]/div/div[1]
    Wait Until Element Is Visible       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[3]/div/div/div[1]/div/div[1]
    Click Element                       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[3]/div/div/div[1]/div/div[1]
    Wait Until Element Is Visible       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div[2]/div
    Click Element                       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div[2]/div/div[2]
    ${InfoBill}                         Get Text    //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[3]/div/div/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]
    Should Not Be True                  """${InfoBill}""" == """########"""
    Close
Exchange Functionality
    LogIN
    Wait Until Element is Visible       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[1]/div[2]/div/div/div[2]/div/div[2]/a/button/div/div/div
    Click Element                       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[1]/div[2]/div/div/div[2]/div/div[2]/a/button/div/div/div
    #In Exchange
    Wait until Element is Visible       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/h2
    Wait until Element is Visible       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div/div[1]/div/div[1]/div[1]/div[2]
    Click Element                       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div/div[1]/div/div[1]/div[1]/div[2]
    Click Element                       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div/div[2]/div/div[1]
    Click Element                       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div[2]/div/div[1]/div/div[1]/div/div[1]/div[1]/div[2]
    Wait Until Element is Visible       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div[2]/div/div[1]/div/div[2]/div/div[2]
    #Input text 10
    Click Element                       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div[2]/div/div[1]/div/div[2]/div/div[2]
    Input text                          //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div/div/div/div[2]/input    ${ExchangeInput}
    #SUM
    Wait Until Element is visible       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div[2]/div/div[2]/div/div/div/div/div[2]/input
    Wait Until Element is visible       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div[2]/div/div[1]/div/div[1]/div/div[1]/div[1]/div[2]/div
    ${ValueAcc}                         Get Text    //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div[2]/div/div[1]/div/div[1]/div/div[1]/div[1]/div[2]/div
    ${ValueAccRemove}                   Remove String     ${ValueAcc}   ,     £
    ${ValueAccNumber}                   Convert To Number   ${ValueAccRemove}
    ${PAIN}                             Evaluate    ${ValueAccNumber}+${ExchangeInput}
    #Click ob Continue and transfer
    Wait Until Element is Visible       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div[2]/div/div[3]/button
    Click Element                       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div[2]/div/div[3]/button
    Input text                          //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[3]/div[2]/div/div/div[1]/div[2]/div[3]/label/div[2]/div/input   test
    Click Element                       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/div[2]/div[2]/div/div[3]/button
    Wait Until Element is Visible       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/button
    Click Element                       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div/button
    #Ověření že platba prošla
    Wait Until Element is Visible       //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/a/div/div/div[2]
    ${AccNumber}                        Get Text    //*[@id="app"]/div/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[2]/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/a/div/div/div[2]
    ${AccNumber}                        Remove String     ${AccNumber}   ,     £
    ${PAIN}                             Convert to Number   ${PAIN}
    ${AccNumber}                        Convert to Number   ${AccNumber}
    Should Be true                      """${AccNumber}""" == """${PAIN}"""
    Close
Close
    Close Browser
*** Test Cases ***
TC02 - LogIn
    LogIN
    Close
TC04 - Check Total Balance in different currencies
    Click and Change Currency
TC05 - Top up Functionality
    Top up functionality
TC06 - Exchange Functionality
    Exchange Functionality